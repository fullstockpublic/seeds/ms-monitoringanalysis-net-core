﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MonitoringAnalysis.Api.Helper;
using MonitoringAnalysis.Business.Abstract;
using MonitoringAnalysis.Business.Service;
using MonitoringAnalysis.Dal;
using MonitoringAnalysis.Dao;
using MonitoringAnalysis.Dao.Abstract;
using MonitoringAnalysis.Migrations.Seed;
using Swashbuckle.AspNetCore.Swagger;
using System;

namespace MonitoringAnalysis.api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public ILoggerFactory LoggerFactory;

        public Startup(IConfiguration configuration, ILoggerFactory LoggerFactory)
        {
            this.LoggerFactory = LoggerFactory;
            this.Configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "MonitoringAnalysis - Api",
                    Description = "Application that captures the values of the country's sensors.",
                    Contact = new Contact { Name = "Linkedin - ncatalogna", Email = "nicocatalogna@gmail.com", Url = "https://www.linkedin.com/in/ncatalogna/" }                    
                });

                //Determine base path for the application.
                var basePath = AppDomain.CurrentDomain.BaseDirectory;

                //Set the comments path for the swagger json and ui.
                options.IncludeXmlComments(basePath + "MonitoringAnalysis.Api.xml");
            });

            services.AddDbContext<ManagerContext>(options => {
                //options.UseLoggerFactory(this.LoggerFactory);
                options.UseInMemoryDatabase(databaseName: "MonitoringAnalysis");
            }, ServiceLifetime.Transient, ServiceLifetime.Singleton);

            services.AddTransient<IInvoiceUoW, InvoiceUoW>();
                       
            // Logging
            services.AddLogging();

            // Instancio Servicio  
            services.AddScoped<ISensorRegistrationService, SensorRegistrationService>();
            services.AddScoped<ISensorMessageService, SensorMessageService>();
            services.AddScoped<IInformedEventService, InformedEventService>();

            services.AddTransient<ICalculationsService, CalculationsService>();

            // Instancio HotService 
            services.AddHostedService<ProcessingHostService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseStaticFiles();

            app.UseCookiePolicy();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseHttpsRedirection();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
