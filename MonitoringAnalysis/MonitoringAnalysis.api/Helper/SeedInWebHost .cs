﻿

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using MonitoringAnalysis.Dal;
using MonitoringAnalysis.Migrations.Seed;

namespace MonitoringAnalysis.Api.Helper
{
    public static class SeedInWebHost
    {
        public static IWebHost Seed(this IWebHost webhost)
        {
            using (var scope = webhost.Services.GetService<IServiceScopeFactory>().CreateScope())
            {
                // alternatively resolve UserManager instead and pass that if only think you want to seed are the users     
                using (var dbContext = scope.ServiceProvider.GetRequiredService<ManagerContext>())
                {
                    new StartAplicationSeed(dbContext);
                }
            }

            return webhost;
        }

        public static void SeedData(this IApplicationBuilder builder)
        {
            var provider = builder.ApplicationServices;
            var scopeFactory = provider.GetRequiredService<IServiceScopeFactory>();

            using (var scope = scopeFactory.CreateScope())
            using (var context = scope.ServiceProvider.GetRequiredService<ManagerContext>())
            {
                new StartAplicationSeed(context);
            }
        }
    }
}
