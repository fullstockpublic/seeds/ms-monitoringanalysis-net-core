﻿using Microsoft.AspNetCore.Http;
using MonitoringAnalysis.Business.Exceptions;
using MonitoringAnalysis.Dao.Abstract;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Threading.Tasks;

namespace MonitoringAnalysis.Api.Middleware
{
    /// <summary>
    /// Middleware Load Post UseIdentityServer And UseAuthentication in startUp
    /// </summary>
    public class GeneralHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public GeneralHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;

        }

        public async Task Invoke(HttpContext context, IInvoiceUoW invoiceUoW)
        {
            try
            {               

                await next(context);

            }
            catch (ControlledExceptions ex)
            {
                invoiceUoW.RollBack();
                await HandleControlledExceptionsAsync(context, ex);
            }
            catch (Exception ex)
            {
                invoiceUoW.RollBack();
                await HandleExceptionAsync(context, ex);
            }
        }



        private static Task HandleControlledExceptionsAsync(HttpContext context, ControlledExceptions exception)
        {
            // strong typed instance 
            JObject jsonObject = new JObject();

            JArray jarrayObj = new JArray();
            jarrayObj.Add(exception.Message);

            // you can explicitly add values here using class interface
            if (string.IsNullOrEmpty(exception.key))
            {
                jsonObject.Add("error", jarrayObj);
            }
            else
            {
                jsonObject.Add(exception.key, jarrayObj);
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)exception.code;

            return context.Response.WriteAsync(jsonObject.ToString());
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            // strong typed instance 
            JObject jsonObject = new JObject();

            JArray jarrayObj = new JArray();
            jarrayObj.Add(exception.Message);

            jsonObject.Add("error", jarrayObj);

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            return context.Response.WriteAsync(jsonObject.ToString());
        }
    }
}
