﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MonitoringAnalysis.Business.Abstract;
using MonitoringAnalysis.Business.Exceptions;
using MonitoringAnalysis.Business.Models;

namespace MonitoringAnalysis.api.Controllers
{
    [Route("api/[controller]")]
    public class SensorMessageController : Controller
    {
        public readonly ISensorMessageService sensorMessageService;

        public SensorMessageController(ISensorMessageService sensorMessageService)
        {
            this.sensorMessageService = sensorMessageService;
        }

        /// <summary>
        /// Test Controller
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {            
            return Ok(new { id = "OK" });
        }

        /// <summary>
        /// Registrar valores de los sensores.
        /// </summary>
        /// <param name="modelEvent">Contains sensor information.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ModelEvent modelEvent)
        {
            if (modelEvent == null)
                ModelState.AddModelError("Info", "Model Event is " + GeneralErrors.NoNullValue.ToString());

            if (modelEvent.sensor == null)
                ModelState.AddModelError("Info", "Model Event sensor is " + GeneralErrors.NoNullValue.ToString());

            if (modelEvent.value == default(int))
                ModelState.AddModelError("Info", "Model Event value is " + GeneralErrors.NoNullValue.ToString());

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Guid idRegister = await this.sensorMessageService.Registration(modelEvent.sensor, modelEvent.value);
            return Ok(new { id = idRegister });
        }

    }
}
