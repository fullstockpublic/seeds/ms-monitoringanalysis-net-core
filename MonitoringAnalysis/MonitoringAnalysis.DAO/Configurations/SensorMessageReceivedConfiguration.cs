﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MonitoringAnalysis.Dal;
using MonitoringAnalysis.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonitoringAnalysis.Dao.Configurations
{
    public class SensorMessageReceivedConfiguration :  IEntityTypeConfiguration<SensorMessageReceived>
    {
        public void Configure(EntityTypeBuilder<SensorMessageReceived> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Value);
            builder.Property(c => c.Date);
            builder.HasOne<Sensor>(c => c.SensorRegistration)
                .WithMany(e => e.SensorMessageReceiveds)
                .IsRequired();
            builder.HasMany<InformedEvent>(x => x.informedEvent);
        }
    }
}
