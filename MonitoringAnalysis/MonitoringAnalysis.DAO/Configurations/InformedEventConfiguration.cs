﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MonitoringAnalysis.Dal;
using MonitoringAnalysis.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonitoringAnalysis.Dao.Configurations
{
    public class InformedEventConfiguration : IEntityTypeConfiguration<InformedEvent>
    {
        public void Configure(EntityTypeBuilder<InformedEvent> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.EventName).HasMaxLength(200);
            builder.Property(c => c.Reporter);
            builder.Property(c => c.CessationUser);
            builder.HasMany<SensorMessageReceived>(x => x.SensorMessageReceiveds);
                

        }
    }
}
