﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MonitoringAnalysis.Dal;
using MonitoringAnalysis.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonitoringAnalysis.Dao.Configurations
{
    public class SensorRegistrationConfiguration : IEntityTypeConfiguration<Sensor>
    {
        public void Configure(EntityTypeBuilder<Sensor> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Name).HasMaxLength(200);
            builder.Property(c => c.Location).HasMaxLength(250);
            builder.Property(c => c.CessationUser);
            builder.HasMany<SensorMessageReceived>(x => x.SensorMessageReceiveds)
                .WithOne(x => x.SensorRegistration);
        }
    }
}
