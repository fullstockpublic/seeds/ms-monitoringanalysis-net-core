﻿using MonitoringAnalysis.Dal.Abstract;
using MonitoringAnalysis.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonitoringAnalysis.Dao.Repositories.Abstract
{
    public interface ISensorMessageReceivedRepository : IMasterRepository<SensorMessageReceived, Guid>
    {
        SensorMessageReceived GetTheMinimum();
        SensorMessageReceived GetTheMaximum();
        double GetAverage();
    }
}
