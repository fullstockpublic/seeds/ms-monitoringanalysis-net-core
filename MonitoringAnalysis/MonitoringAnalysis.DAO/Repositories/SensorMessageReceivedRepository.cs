﻿using MonitoringAnalysis.Dal;
using MonitoringAnalysis.Dal.Abstract;
using MonitoringAnalysis.Dao.Repositories.Abstract;
using MonitoringAnalysis.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonitoringAnalysis.Dao.Repositories
{
    public class SensorMessageReceivedRepository : MasterRepository<SensorMessageReceived, Guid>, ISensorMessageReceivedRepository
    {
        public SensorMessageReceivedRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public SensorMessageReceived GetTheMaximum()
        {
            SensorMessageReceived result = this.DbSet
                        .Where(x => x.Status == false)
                        .OrderByDescending(x => x.Value)
                        //.ThenByDescending(x => x.Value)
                        .First();
            
            return result;
        }

        public SensorMessageReceived GetTheMinimum()
        {
            SensorMessageReceived result = this.DbSet
                        .Where(x => x.Status == false)
                        .OrderBy(x => x.Value)
                        //.ThenBy(x => x.Value)
                        .First();
            return result;
        }

        public double GetAverage()
        {
            double result = this.DbSet
                        .Select(x => x.Value).Average();
            return result;
        }
        
    }
}
