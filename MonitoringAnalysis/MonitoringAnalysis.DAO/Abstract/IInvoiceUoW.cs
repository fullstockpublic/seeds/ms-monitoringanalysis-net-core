﻿using MonitoringAnalysis.Dal.Abstract;
using MonitoringAnalysis.Dao.Repositories.Abstract;
using MonitoringAnalysis.Domain.Entities;
using System;

namespace MonitoringAnalysis.Dao.Abstract
{
    public interface IInvoiceUoW : IUnitOfWork
    {
        ISensorMessageReceivedRepository SensorMessageReceivedRepository { get; }

        IMasterRepository<Sensor, Guid> SensorRepository { get; }
        IMasterRepository<InformedEvent, Guid> InformedEvent { get; }
    }
}
