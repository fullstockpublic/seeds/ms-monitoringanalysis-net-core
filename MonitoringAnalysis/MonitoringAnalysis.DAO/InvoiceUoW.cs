﻿using Microsoft.EntityFrameworkCore;
using MonitoringAnalysis.Dal;
using MonitoringAnalysis.Dal.Abstract;
using MonitoringAnalysis.Dao.Abstract;
using MonitoringAnalysis.Dao.Repositories;
using MonitoringAnalysis.Dao.Repositories.Abstract;
using MonitoringAnalysis.Domain.Entities;
using System;
using System.Data.Common;

namespace MonitoringAnalysis.Dao
{
    public class InvoiceUoW : UnitOfWork, IInvoiceUoW
    {
        public InvoiceUoW(ManagerContext dbContextfactory) : base(dbContextfactory)
        {
            // Repository Not Generic
            SensorMessageReceivedRepository = new SensorMessageReceivedRepository(this);

            // Repository Generic
            SensorRepository = new MasterRepository<Sensor, Guid>(this);
            InformedEvent = new MasterRepository<InformedEvent, Guid>(this);
            
        }

        public ISensorMessageReceivedRepository SensorMessageReceivedRepository { get; private set; }

        public IMasterRepository<Sensor, Guid> SensorRepository { get; private set; }
        public IMasterRepository<InformedEvent, Guid> InformedEvent { get; private set; }
    }
}
