﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MonitoringAnalysis.Business.Abstract
{
    public interface ISensorRegistrationService
    {
        Task<Guid> Registration(string name, string location);
    }
}
