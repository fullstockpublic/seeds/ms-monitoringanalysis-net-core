﻿using MonitoringAnalysis.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonitoringAnalysis.Business.Abstract
{
    public interface IInformedEventService
    {
        IEnumerable<InformedEvent> GetAll();
        IEnumerable<InformedEvent> Get(Guid idSensor);
    }
}
