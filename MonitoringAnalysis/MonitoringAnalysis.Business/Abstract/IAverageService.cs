﻿using MonitoringAnalysis.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonitoringAnalysis.Business.Abstract
{
    public interface ICalculationsService
    {
        void Constancy();
        void Average();
    }
}
