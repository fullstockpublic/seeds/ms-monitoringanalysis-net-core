﻿using System;
using System.Threading.Tasks;

namespace MonitoringAnalysis.Business.Abstract
{
    public interface ISensorMessageService
    {
        Task<Guid> Registration(Guid sensor, int value);
    }
}
