﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MonitoringAnalysis.Business.Service
{
    /// <summary>
    /// Basis for asynchronous work, we used the following example that allows abstraction.
    /// https://stackoverflow.com/questions/51616000/how-do-i-call-a-method-from-a-singleton-service-to-run-throughout-the-applicatio?answertab=active#tab-top
    /// </summary>
    public abstract class HostedService : IHostedService
    {
        private Task executingTask;
        private CancellationTokenSource cancellationTokenSource;

        public Task StartAsync(CancellationToken cancellationToken)
        {
            this.cancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);

            this.executingTask = this.ExecuteAsync(this.cancellationTokenSource.Token);

            return this.executingTask.IsCompleted ? this.executingTask : Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (this.executingTask == null)
            {
                return;
            }

            this.cancellationTokenSource.Cancel();

            await Task.WhenAny(this.executingTask, Task.Delay(-1, cancellationToken));
        }

        protected abstract Task ExecuteAsync(CancellationToken cancellationToken);
    }
}
