﻿using MonitoringAnalysis.Business.Abstract;
using MonitoringAnalysis.Dao.Abstract;
using MonitoringAnalysis.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MonitoringAnalysis.Business.Service
{
    public class SensorRegistrationService : ISensorRegistrationService
    {
        readonly IInvoiceUoW uoW;

        public SensorRegistrationService(IInvoiceUoW uoW)
        {
            this.uoW = uoW;
        }

        public async Task<Guid> Registration(string name, string location)
        {
            Sensor sensorRegistration = new Sensor()
            {
                Id = Guid.NewGuid(),
                Name = name,
                Location = location                
            };

            this.uoW.SensorRepository.Create(sensorRegistration);

            await this.uoW.CommitAsync();

            return sensorRegistration.Id;
        }
    }
}
