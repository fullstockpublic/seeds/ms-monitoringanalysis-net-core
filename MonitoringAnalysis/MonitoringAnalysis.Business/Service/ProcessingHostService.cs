﻿using Microsoft.Extensions.Logging;
using MonitoringAnalysis.Business.Abstract;
using MonitoringAnalysis.Dao.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MonitoringAnalysis.Business.Service
{
    public class ProcessingHostService : HostedService
    {
        readonly  ILogger<ProcessingHostService> logger;
        readonly  ICalculationsService averageService;

        public ProcessingHostService(ILogger<ProcessingHostService> logger, ICalculationsService averageService)
        {
            this.logger = logger;
            this.averageService = averageService;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            // Run something
            while (!cancellationToken.IsCancellationRequested)
            {
                this.logger.LogInformation("*******[ ] Start The calculation: " + DateTime.Now.ToLongTimeString());

                this.averageService.Constancy();               

                this.averageService.Average();

                this.logger.LogInformation("*******[ ] Finish The calculation: " + DateTime.Now.ToLongTimeString());

                //Waiting time of x, to maintain the processing order of 2 per minute
                int time = Convert.ToInt32(Environment.GetEnvironmentVariable("TIMEAVERAGE"));
                await Task.Delay(time, cancellationToken); //60000 Minute
            }
 
        }
    }
}
