﻿using MonitoringAnalysis.Business.Abstract;
using MonitoringAnalysis.Business.Exceptions;
using MonitoringAnalysis.Dao.Abstract;
using MonitoringAnalysis.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace MonitoringAnalysis.Business.Service
{
    public class SensorMessageService : ISensorMessageService
    {
        readonly IInvoiceUoW uoW;

        public SensorMessageService(IInvoiceUoW uoW)
        {
            this.uoW = uoW;
        }

        public async Task<Guid> Registration(Guid idSensor, int value)
        {
            Sensor sensor = this.uoW.SensorRepository.Get(idSensor);

            if (sensor == null)            
                throw new ControlledExceptions(System.Net.HttpStatusCode.Conflict, "The Sensor id sent, does not correspond to any registered.");

            SensorMessageReceived sensorMessageReceived = new SensorMessageReceived()
            {
                Id = Guid.NewGuid(),
                Value = value,
                SensorRegistration = sensor,
                Date = DateTime.Now,
                Status = false
            };

            this.uoW.SensorMessageReceivedRepository.Create(sensorMessageReceived);

            await this.uoW.CommitAsync();

            return sensorMessageReceived.Id;
        }
    }
}
