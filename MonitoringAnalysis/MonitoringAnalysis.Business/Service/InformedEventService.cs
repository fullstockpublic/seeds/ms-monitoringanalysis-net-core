﻿using MonitoringAnalysis.Business.Abstract;
using MonitoringAnalysis.Business.Exceptions;
using MonitoringAnalysis.Dao.Abstract;
using MonitoringAnalysis.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonitoringAnalysis.Business.Service
{
    public class InformedEventService : IInformedEventService
    {
        readonly IInvoiceUoW uoW;

        public InformedEventService(IInvoiceUoW uoW)
        {
            this.uoW = uoW;
        }

        public IEnumerable<InformedEvent> Get(Guid idSensor)
        {
            SensorMessageReceived sensorMessageReceived = this.uoW.SensorMessageReceivedRepository.SingleOrDefault(x => x.SensorRegistration.Id.Equals(idSensor));

            if (sensorMessageReceived == null)
            {
                throw new ControlledExceptions(System.Net.HttpStatusCode.Conflict, "idSensor parameter is not registered or non-existent. ");
            }

            return this.uoW.InformedEvent.Find(x => x.SensorMessageReceiveds.Contains(sensorMessageReceived));
        }

        public IEnumerable<InformedEvent> GetAll()
        {
            return this.uoW.InformedEvent.GetAll();
        }

    }
}
