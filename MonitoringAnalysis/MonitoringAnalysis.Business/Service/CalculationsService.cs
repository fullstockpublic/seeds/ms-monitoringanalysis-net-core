﻿using Microsoft.Extensions.Logging;
using MonitoringAnalysis.Business.Abstract;
using MonitoringAnalysis.Dao.Abstract;
using MonitoringAnalysis.Domain.Entities;
using System;

namespace MonitoringAnalysis.Business.Service
{
    public class CalculationsService : ICalculationsService
    {
        readonly IInvoiceUoW uoW;
        readonly ILogger<CalculationsService> logger;

        public CalculationsService(IInvoiceUoW uoW, ILogger<CalculationsService> logger)
        {
            this.uoW = uoW;
            this.logger = logger;
        }

        public void Average()
        {
            //Registremos
            int mConstante = Convert.ToInt32(Environment.GetEnvironmentVariable("M-CONSTANTE"));

            //Calcular Promedio
            double avarage = this.uoW.SensorMessageReceivedRepository.GetAverage();

            if (avarage > mConstante)
            {
                InformedEvent eventInfo = new InformedEvent() { Id = Guid.NewGuid(), EventName = "average", value = avarage };

                this.uoW.InformedEvent.Create(eventInfo);

                this.uoW.Commit();

                this.logger.LogInformation($"*******[X] An average {mConstante} greater than {avarage} ");
            }

            
        }

        public void Constancy()
        {
            //Registremos
            int sConstante = Convert.ToInt32(Environment.GetEnvironmentVariable("S-CONSTANTE"));

            //Calcular Diferencia
            //Obtener maximo
            var sensorMessageMaximum = this.uoW.SensorMessageReceivedRepository.GetTheMaximum();
                        
            //Obtener Minimo
            var sensorMessageMinimum = this.uoW.SensorMessageReceivedRepository.GetTheMinimum();
                        
            int calculate = sensorMessageMaximum.Value - sensorMessageMinimum.Value;

            if (calculate > sConstante)
            {
                InformedEvent eventInfo = new InformedEvent() { Id = Guid.NewGuid(), EventName = "constancy", value = calculate };
                eventInfo.SensorMessageReceiveds.Add(sensorMessageMaximum);
                eventInfo.SensorMessageReceiveds.Add(sensorMessageMinimum);

                this.uoW.InformedEvent.Create(eventInfo);

                this.uoW.Commit();

                this.logger.LogInformation($"*******[X] A Difference in constancy was detected {sConstante} ", ConsoleColor.Blue);
                this.logger.LogInformation($"*******[X] It is calculated {calculate} between the minimum {sensorMessageMinimum.Value} and maximum {sensorMessageMaximum.Value} ", ConsoleColor.Blue);
                this.logger.LogInformation($"*******[X] Between the minimum {sensorMessageMinimum.SensorRegistration.Name} and maximum {sensorMessageMaximum.SensorRegistration.Name} sensors ", ConsoleColor.Blue);
            } 
        }
    }
}
