﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonitoringAnalysis.Business.Exceptions
{
    public enum GeneralErrors
    {
        WrongFilters = 1,
        BiggerDateTo = 2,
        InvalidDateFromFormat = 3,
        InvalidDateRange = 4,
        InvalidDateToFormat = 5,
        NoPermissionOrUser = 6,
        NoNullValue = 7,
        InvalidEmail = 8,
        InvaliIdMustBeNull = 9,
        InvalidIdMustNotBeNull = 10,
    }
}
