﻿using System;
using System.Collections.Generic;
using System.Net;

namespace MonitoringAnalysis.Business.Exceptions
{
    public class ControlledExceptions : Exception
    {
        public HttpStatusCode code;
        public string key;
        public Dictionary<string, string> messageList;

        public ControlledExceptions()
        {
            this.code = HttpStatusCode.InternalServerError;
        }

        public ControlledExceptions(HttpStatusCode code)
        {
            this.code = code;
        }

        public ControlledExceptions(HttpStatusCode code, Dictionary<string, string> messageList) : base()
        {
            this.code = code;
            this.messageList = messageList;
        }

        public ControlledExceptions(HttpStatusCode code, string message) : base(message)
        {
            this.code = code;
            this.key = string.Empty;
        }

        public ControlledExceptions(HttpStatusCode code, string message, Exception inner) : base(message)
        {
            this.code = code;
            this.key = string.Empty;
        }

        public ControlledExceptions(HttpStatusCode code, string message, string key) : base(message)
        {
            this.code = code;
            this.key = key;
        }

        public ControlledExceptions(HttpStatusCode code, string message, string key, Exception inner) : base(message)
        {
            this.code = code;
            this.key = key;
        }
    }
}
