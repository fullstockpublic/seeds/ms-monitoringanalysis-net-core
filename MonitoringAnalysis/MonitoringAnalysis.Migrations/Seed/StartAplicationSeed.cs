﻿using MonitoringAnalysis.Dal;
using MonitoringAnalysis.Domain.Entities;
using MonitoringAnalysis.Migrations.help;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MonitoringAnalysis.Migrations.Seed
{
    public class StartAplicationSeed
    {
        ManagerContext context;

        public StartAplicationSeed(ManagerContext context)
        {
            this.context = context;
            
            //Entities
            AddSensors();
            this.context.SaveChangesAsync().GetAwaiter().GetResult();

            AddMessages();
            this.context.SaveChangesAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// https://es.wikipedia.org/wiki/Anexo:Estaciones_meteorol%C3%B3gicas_en_Argentina
        /// </summary>
        private void AddSensors()
        {
            var sensor = this.context.Set<Sensor>();

            sensor.AddOrUpdate(x => x.Id,
                    new Sensor()
                    {
                        Id = new Guid("c49a6dfd-67a9-4cd5-8d45-4342b596854c"),
                        Name = "Base Belgrano II",
                        Location = "Antártida Argentina"
                    },
                    new Sensor()
                    {
                        Id = new Guid("8ff12273-5c76-49b0-990a-cb79bc36b80c"),
                        Name = "Azul Aero",
                        Location = "Buenos Aires"
                    },
                    new Sensor()
                    {
                        Id = new Guid("f681bb4b-be6e-41c7-b770-770dc181fd77"),
                        Name = "Aeroparque Buenos Aires",
                        Location = "Ciudad Autónoma de Buenos Aires"
                    },
                    new Sensor()
                    {
                        Id = new Guid("c1b3d1c5-06a4-4a02-ad4c-455e1b700dae"),
                        Name = "Córdoba Aero",
                        Location = "Córdoba"
                    }
                );
            
        }

        /// <summary>
        /// Basic Init
        /// </summary>
        private void AddMessages()
        {
            var sensorMessage = this.context.Set<SensorMessageReceived>();
            var sensore = this.context.Set<Sensor>();
            var codroba = sensore.Where(x => x.Id == new Guid("c1b3d1c5-06a4-4a02-ad4c-455e1b700dae")).FirstOrDefault();
            
            sensorMessage.AddOrUpdate(x => x.Id,
                    new SensorMessageReceived()
                    {
                        Id = new Guid("c49a6dfd-67a9-4cd5-8d45-4342b596854c"),
                        Status = false,
                        Value = 0,
                        SensorRegistration = codroba
                    }
                );

        }
    }

}
