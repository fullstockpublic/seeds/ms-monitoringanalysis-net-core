using Bogus;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MonitoringAnalysis.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MonitoringAnalysis.UnitTest
{
    [TestClass]
    public class UnitTest1 : Context
    {
        [TestMethod]
        public void ValidateSensoCreation()
        {            
            string name = this.faker.Name.FullName();
            string location = this.faker.Address.Direction();

            Guid id = this.sensorRegistrationService.Registration(name, location).GetAwaiter().GetResult();

            this.invoiceUoW.Commit();

            Sensor sensor = this.invoiceUoW.SensorRepository.Get(id);

            Assert.IsTrue(name == sensor.Name);
            Assert.IsTrue(location == sensor.Location);
        }
        
        [TestMethod]
        [DataRow(10)]
        [DataRow(25)]
        [DataRow(50)]
        public void AddMessageFromSensor(int value)
        {
            Guid registry = this.sensorMessageService.Registration(
                            new Guid("8ff12273-5c76-49b0-990a-cb79bc36b80c"),
                            this.faker.Random.Number(-10, value)).GetAwaiter().GetResult();

            this.invoiceUoW.Commit();

            SensorMessageReceived sensorMessage = this.invoiceUoW.SensorMessageReceivedRepository.Get(registry);

            Assert.IsTrue(registry == sensorMessage.Id); 
        }

        [TestMethod]
        public void ValidateAverageCalculationsService()
        {
            this.sensorMessageService.Registration(
                            new Guid("8ff12273-5c76-49b0-990a-cb79bc36b80c"),
                            200).GetAwaiter().GetResult();

            this.sensorMessageService.Registration(
                   new Guid("8ff12273-5c76-49b0-990a-cb79bc36b80c"),
                   200).GetAwaiter().GetResult();

            this.invoiceUoW.Commit();

            this.averageService.Average();

            this.invoiceUoW.Commit();

            List<InformedEvent> informedEvents = this.invoiceUoW.InformedEvent.Find(x => x.EventName.Contains("average")).ToList();

            Assert.IsTrue(informedEvents.Count > 0);
        }

        [TestMethod]
        public void ValidateConstancyCalculationsService()
        {
            this.sensorMessageService.Registration(
                        new Guid("8ff12273-5c76-49b0-990a-cb79bc36b80c"),
                        10).GetAwaiter().GetResult();

            this.sensorMessageService.Registration(
                            new Guid("8ff12273-5c76-49b0-990a-cb79bc36b80c"),
                            20).GetAwaiter().GetResult();

            this.sensorMessageService.Registration(
                                new Guid("8ff12273-5c76-49b0-990a-cb79bc36b80c"),
                                30).GetAwaiter().GetResult();

            this.sensorMessageService.Registration(
                                    new Guid("8ff12273-5c76-49b0-990a-cb79bc36b80c"),
                                    40).GetAwaiter().GetResult();

            this.sensorMessageService.Registration(
                                        new Guid("8ff12273-5c76-49b0-990a-cb79bc36b80c"),
                                        200).GetAwaiter().GetResult();

            this.invoiceUoW.Commit();

            this.averageService.Constancy();

            this.invoiceUoW.Commit();

            List<InformedEvent> informedEvents = this.invoiceUoW.InformedEvent.Find(x => x.EventName.Contains("constancy")).ToList();

            Assert.IsTrue(informedEvents.Count > 0);
        }
    }
}
