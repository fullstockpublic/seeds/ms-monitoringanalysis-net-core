﻿using Bogus;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MonitoringAnalysis.Business.Abstract;
using MonitoringAnalysis.Business.Service;
using MonitoringAnalysis.Dal;
using MonitoringAnalysis.Dao;
using MonitoringAnalysis.Dao.Abstract;
using MonitoringAnalysis.Migrations.Seed;

namespace MonitoringAnalysis.UnitTest
{
    public class Context
    {
        public IInvoiceUoW invoiceUoW;
        public Faker faker;

        public ISensorMessageService sensorMessageService;
        public ISensorRegistrationService sensorRegistrationService;
        public ICalculationsService averageService;
        public Context()
        {
            this.faker = new Faker();
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .BuildServiceProvider();

            var factory = serviceProvider.GetService<ILoggerFactory>();

            var logger = factory.CreateLogger<CalculationsService>();

            var optionsBuilder = new DbContextOptionsBuilder<ManagerContext>().UseInMemoryDatabase(databaseName: "MonitoringAnalysis").Options;

            ManagerContext managerContext = new ManagerContext(optionsBuilder);

            new StartAplicationSeed(managerContext);

            // Seed
            //new InitialSeed(instance);

            // Instancio Uow
            invoiceUoW = new InvoiceUoW(managerContext);

            // Repository
            //userRepository = new UserRepository(invoiceUoW);
            //userActionRepository = new UserActionRepository(invoiceUoW);

            // Service
            this.sensorMessageService = new SensorMessageService(invoiceUoW);
            this.averageService = new CalculationsService(invoiceUoW, logger);
            this.sensorRegistrationService = new SensorRegistrationService(invoiceUoW);
        }
    }
}
