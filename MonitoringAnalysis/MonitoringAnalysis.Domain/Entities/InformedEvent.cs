﻿using MonitoringAnalysis.Dal;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonitoringAnalysis.Domain.Entities
{
    public class InformedEvent : BaseEntity<Guid>
    {
        public InformedEvent() => SensorMessageReceiveds = new HashSet<SensorMessageReceived>();
        public string EventName { get; set; }
        public DateTime Reporter { get; set; }
        public double value { get; set; }
        public virtual ICollection<SensorMessageReceived> SensorMessageReceiveds { get; set; }
    }
}
