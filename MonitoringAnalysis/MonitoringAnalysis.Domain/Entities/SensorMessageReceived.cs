﻿using MonitoringAnalysis.Dal;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonitoringAnalysis.Domain.Entities
{
    public class SensorMessageReceived : BaseEntity<Guid>
    {
        public SensorMessageReceived() => informedEvent = new HashSet<InformedEvent>();
        public virtual DateTime Date { get; set; }
        public virtual int Value { get; set; }
        public virtual bool Status { get; set; }        
        public virtual Sensor SensorRegistration { get; set; }
        public virtual ICollection<InformedEvent> informedEvent { get; set; }        
    }
}
