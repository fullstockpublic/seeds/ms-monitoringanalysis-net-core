﻿using MonitoringAnalysis.Dal;
using System;
using System.Collections.Generic;

namespace MonitoringAnalysis.Domain.Entities
{
    public class Sensor : BaseEntity<Guid>
    {
        public Sensor() => SensorMessageReceiveds = new HashSet<SensorMessageReceived>();

        public virtual string Location { get; set; }
        public virtual string Name { get; set; }
        public virtual ICollection<SensorMessageReceived> SensorMessageReceiveds { get; set; }
    }
}
