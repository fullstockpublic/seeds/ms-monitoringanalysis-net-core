﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MonitoringAnalysis.Dal.Abstract;

namespace MonitoringAnalysis.Dal
{
    public abstract class EntityMappingConfiguration<T> : IEntityMappingConfigurationGenerict<T> where T : class
    {
        public abstract void Map(EntityTypeBuilder<T> b);

        public void Map(ModelBuilder b)
        {
            Map(b.Entity<T>());
        }
    }
}
