﻿using Microsoft.EntityFrameworkCore;
using MonitoringAnalysis.Dal.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MonitoringAnalysis.Dal
{
    /// <summary>
    /// Generic repository pattern implementation
    /// Repository  Mediates between the domain and data mapping layers using a collection-like interface for accessing domain objects.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    /// <seealso cref="SIR.DataAccessLayer.Repository.Generic.IMasterRepository{TEntity, TKey}" />
    public class MasterRepository<TEntity, TKey> : IMasterRepository<TEntity, TKey> where TEntity : class
    {

        /// <summary>
        /// DbSet is part of EF, it holds entities of the context in memory, per EF guidelines DbSet was used instead of IDbSet 
        /// </summary>        
        private readonly DbSet<TEntity> _dbSet;

        /// <summary>
        ///     Ef's DbSet - in-memory collection for dealing with entities
        /// </summary>
        public DbSet<TEntity> DbSet { get { return _dbSet; } }

        /// <summary>
        /// UnitOfWork is part of EF, contains context and other generic properties
        /// </summary>   
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="MasterRepository{TEntity, TKey}"/> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <exception cref="ArgumentNullException">unitOfWork - Unit of work cannot be null</exception>
        public MasterRepository(IUnitOfWork unitOfWork)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork), @"Unit of work cannot be null");

            this._unitOfWork = unitOfWork;
            _dbSet = unitOfWork.DatabaseContext.Set<TEntity>();
        }

        /// <summary>
        /// Gets entity with given key
        /// </summary>
        /// <param name="id">The key of the entity</param>
        /// <returns>Entity with key id</returns>
        virtual public TEntity Get(TKey id)
        {
            return _dbSet.Find(id);
        }

        /// <summary>
        /// Asynchronously gets entity with given key
        /// </summary>
        /// <param name="id">The key of the entity</param>
        /// <returns>Entity with key id</returns>
        virtual public async Task<TEntity> GetAsnyc(TKey id)
        {
            return await _dbSet.FindAsync(id);
        }

        /// <summary>
        /// Gets all entities
        /// </summary>
        /// <returns>List of entities of type TEntiy</returns>
        virtual public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        /// <summary>
        /// Gets all entities Async
        /// </summary>
        /// <returns>List of entities of type TEntiy Async</returns>
        virtual public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        /// <summary>
        /// Find the entity with the predicate parameters
        /// </summary>
        /// <returns>List of entities of type TEntiy</returns>
        virtual public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate).ToList();
        }

        /// <summary>
        /// Find the entity with the predicate parameters
        /// </summary>
        /// <returns>Single Or Default of type TEntiy</returns>
        virtual public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate).SingleOrDefault();
        }

        /// <summary>
        /// Find the entity with the predicate parameters
        /// </summary>
        /// <returns>Queryable of type TEntiy</returns>
        virtual public IQueryable<TEntity> Queryable(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate);
        }

        /// <summary>
        /// Create Designated Entity
        /// </summary>
        virtual public void Create(TEntity entity)
        {
            _dbSet.Add(entity);
        }

        /// <summary>
        /// Create and Ref Designated Entity
        /// </summary>
        virtual public void Create(ref TEntity entity)
        {
            _dbSet.Add(entity);
        }

        /// <summary>
        /// Delete designated entity
        /// </summary>
        virtual public void DeleteFisic(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        /// <summary>
        /// Delete designated entity
        /// </summary>
        virtual public void DeleteLogic(TEntity entity)
        {
            IBaseEntity<TKey> entityd = (IBaseEntity<TKey>)entity;
            entityd.CessationDate = DateTime.Now;

            if (_unitOfWork.UserContext != Guid.Empty)
            {
                entityd.CessationUser = _unitOfWork.UserContext;
            }
            else
            {
                throw new Exception("You want to delete a data without context user, this error is unexpected and you need to validate the logic to prevent it from happening.");
            }

            _dbSet.Add(entity);
        }

        /// <summary>
        /// Saves unitOfWork context
        /// </summary>
        /// <returns>Will return False if successful</returns>
        public bool Save()
        {
            return (_unitOfWork.Commit() >= 0);
        }

    }
}
