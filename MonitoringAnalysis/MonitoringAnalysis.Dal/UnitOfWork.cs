﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using MonitoringAnalysis.Dal.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MonitoringAnalysis.Dal
{
    /// <summary>
    /// This is implementation of UoW pattern
    /// </summary>
    /// <remarks>
    /// "Maintains a list of objects affected by a business transaction and coordinates the writing out of changes and the resolution of concurrency problems."
    /// According to P of EEA, Unit of work should have following methods: commit(), registerNew((object), registerDirty(object), registerClean(object), registerDeleted(object)
    /// The thing is DbContext is already implementation of UoW so there is no need to implement all this
    /// </remarks>
    /// <seealso cref="T:UnitOfWork.IUnitOfWork" />
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Is instance already disposed
        /// </summary>
        /// <remarks>
        /// Default value of bool is false
        /// </remarks>
        private bool _disposed;


        public Guid UserContext { get; private set; }

        /// <summary>
        /// Gets the database context. DatabaseContext is part of EF and itself is implementation of UoW (and repo) patterns
        /// </summary>
        /// <value>
        /// The database context.
        /// </value>
        /// <remarks>
        /// If true  UoW was implemented this wouldn't be here, but we are exposing this for simplicity sake.
        /// For example so that repository  could use benefits of DbContext and DbSet <see cref="DbSet" />. One of those benefits are Find and FindAsnyc methods
        /// </remarks>
        public DbContext DatabaseContext { get; }

        /// <summary>
        /// Transaction Warehouse
        /// </summary>
        public IDbContextTransaction Transaction { get; private set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="dbContextfactory">The database context factory.</param>
        /// <exception cref="ArgumentNullException">
        /// dbContextfactory
        /// or
        /// MasterDbContext - Master database context cannot be null
        /// </exception>
        public UnitOfWork(DbContext conn)
        {
            if (conn == null)
                throw new ArgumentNullException(nameof(conn));

            this.DatabaseContext = conn ?? throw new ArgumentNullException(nameof(conn), @"Master database context cannot be null");
        }

        /// <summary>
        /// Enter context user
        /// </summary>
        /// <returns></returns>
        public bool SetUser(Guid userContext)
        {
            this.UserContext = userContext;
            return true;
        }
        /// <summary>
        /// Generate a transaction
        /// </summary>
        /// <returns></returns>
        public IDbContextTransaction BeginTransaction()
        {
            this.Transaction = DatabaseContext.Database.BeginTransaction();
            return this.Transaction;
        }

        /// <summary>
        /// Rollback transactions
        /// </summary>
        public void RollBack()
        {
            Transaction?.Rollback();
            Dispose();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int EndTransaction()
        {
            Transaction?.Commit();
            return Commit();
        }

        /// <summary>
        /// Opening of transmissibility for the processes generated after this instruction. 
        /// This transaction will be finalized when the Middleware or On Demand process is closed.
        /// </summary>
        public int Commit()
        {
            int resultSave = 0;

            if (DatabaseContext.ChangeTracker.HasChanges())
            {
                resultSave = DatabaseContext.SaveChanges();
            }

            return resultSave;
        }

        /// <summary>
        /// Asynchronously commits changes to database.
        /// </summary>
        /// <returns></returns>
        public async Task CommitAsync() =>
            await DatabaseContext.SaveChangesAsync();

        /// <summary>
        /// Rear Overload Shape Load
        /// </summary>
        public virtual void RepositoryLoad()
        {
            throw new NotImplementedException("Required to implement in 'InvoiceUoW' or 'UnitOfWork' The Repository layer, overwriting the 'RepositoryLoad' method");
        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposning"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposning)
        {
            if (_disposed)
                return;

            if (disposning)
            {
                Transaction?.Dispose();
                DatabaseContext.Dispose();
            }

            _disposed = true;
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        ~UnitOfWork() => Dispose(false);
    }
}
