﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Reflection;

namespace MonitoringAnalysis.Dal
{
    public class ManagerContext : DbContext
    {

        /// <summary>
        /// DbContext factory and connection manager in the data model.
        /// </summary>
        /// <param name="con"></param>
        public ManagerContext(DbContextOptions<ManagerContext> options) : base(options)
        {
        }
        
        /// <summary>
        /// The model will be loaded dynamically by searching for an assembler with 'EntityTypeConfiguration', based on the MEF (Managed Extensibility Framework )
        /// More info https://odetocode.com/blogs/scott/archive/2011/11/28/composing-entity-framework-fluent-configurations.aspx
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // The Migration may not load the DAO library into memory, so it will be forced to load all the libraries contained in the project.
            if (System.Diagnostics.Process.GetCurrentProcess().ProcessName.Contains("devenv")) // UnitTest:"testhost." Migration:"devenv"
            {
                var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();
                var loadedPaths = loadedAssemblies.Select(a => a.Location).ToArray();

                var referencedPaths = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll");
                var toLoad = referencedPaths.Where(r => !loadedPaths.Contains(r, StringComparer.InvariantCultureIgnoreCase) && searchLibrary(r)).ToList();
                toLoad.ForEach(path => loadedAssemblies.Add(AppDomain.CurrentDomain.Load(AssemblyName.GetAssemblyName(path))));
            }


            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (Assembly assembly in assemblies)
            {
                //Do not analyze Dlls that are generic to the project
                if (!searchLibrary(assembly.GetName().Name))
                {
                    continue;
                }

                IEnumerable<Type> typeList = assembly.GetTypes().Where(type => type.GetInterfaces().Any(inter =>
                                                       inter.IsGenericType &&
                                                       inter.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>)));

                if (typeList.Count() > 0)
                {
                    foreach (var type in typeList)
                    {
                        dynamic configurationInstance = Activator.CreateInstance(type);
                        modelBuilder.ApplyConfiguration(configurationInstance);
                    }

                    break;
                }

            }
        }
        
        private bool searchLibrary(string Location)
        {
            if (Location.Contains("Windows") || Location.Contains("PROGRAM FILES (X86)")
                || Location.Contains("PROGRAM FILES")
                || Location.Contains("Microsoft.")
                || Location.Contains("System.")
                || Location.Contains("EntityFramework.")
                || Location.Contains("Oracle.")
                || Location.Contains("netstandard.")
                || Location.Contains("Remotion.")
                || Location.Contains("Z.")
                || Location.Contains("testhost")
                )
            {
                return false;
            }
            return true;
        }
    }
}
