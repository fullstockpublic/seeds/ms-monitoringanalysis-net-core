﻿using MonitoringAnalysis.Dal.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonitoringAnalysis.Dal
{
    public abstract class BaseEntity<T> : IBaseEntity<T>
    {
        public virtual T Id { get; set; }
        public virtual DateTime CessationDate { get; set; }
        public virtual Guid CessationUser { get; set; }

    }
}
