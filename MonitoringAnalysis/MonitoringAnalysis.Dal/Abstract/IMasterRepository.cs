﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MonitoringAnalysis.Dal.Abstract
{
    /// <summary>
    /// Generic repository pattern implementation
    /// Repository  Mediates between the domain and data mapping layers using a collection-like interface for accessing domain objects.
    /// </summary>
    /// <remarks>
    /// More info: https://martinfowler.com/eaaCatalog/repository.html
    /// </remarks>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    public interface IMasterRepository<TEntity, in TKey> where TEntity : class
    {

        /// <summary>
        ///     Ef's DbSet - in-memory collection for dealing with entities
        /// </summary>
        DbSet<TEntity> DbSet { get; }

        /// <summary>
        ///     Gets entity (of type) from repository based on given ID
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Entity</returns>
        TEntity Get(TKey id);

        /// <summary>
        /// Asynchronously gets entity (of type) from repository based on given ID
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<TEntity> GetAsnyc(TKey id);

        /// <summary>
        ///     Gets all entities of type from repository
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> GetAll();

        /// <summary>
        ///  Asynchronously gets all entities of type from repository
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<TEntity>> GetAllAsync();

        /// <summary>
        ///     Finds all entities of type which match given predicate
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns>Entities which satisfy conditions</returns>
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Find the entity with the predicate parameters
        /// </summary>
        /// <returns>Single Or Default of type TEntiy</returns>
        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Find the entity with the predicate parameters
        /// </summary>
        /// <returns>Queryable of type TEntiy</returns>
        IQueryable<TEntity> Queryable(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Create Designated Entity
        /// </summary>
        void Create(TEntity entity);

        /// <summary>
        /// Create Ref Designated Entity
        /// </summary>
        void Create(ref TEntity entity);

        /// <summary>
        /// Delete Logic designated entity
        /// </summary>
        void DeleteLogic(TEntity entity);


        /// <summary>
        /// Delete Fisic designated entity
        /// </summary>
        void DeleteFisic(TEntity entity);

        /// <summary>
        /// Saves unitOfWork context
        /// </summary>
        /// <returns>Will return False if successful</returns>
        bool Save();
    }
}
