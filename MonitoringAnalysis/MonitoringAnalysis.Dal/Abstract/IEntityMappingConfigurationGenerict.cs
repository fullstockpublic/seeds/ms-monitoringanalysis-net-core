﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MonitoringAnalysis.Dal.Abstract
{
    interface IEntityMappingConfigurationGenerict<T> : IEntityMappingConfiguration where T : class
    {
        void Map(EntityTypeBuilder<T> builder);
    }
}
