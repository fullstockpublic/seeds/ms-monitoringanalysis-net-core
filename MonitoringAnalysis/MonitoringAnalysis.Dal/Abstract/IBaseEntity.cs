﻿using System;

namespace MonitoringAnalysis.Dal.Abstract
{
    public interface IBaseEntity<T>
    {
        T Id { get; set; }
        DateTime CessationDate { get; set; }
        Guid CessationUser { get; set; }
    }
}
