﻿using Microsoft.EntityFrameworkCore;

namespace MonitoringAnalysis.Dal.Abstract
{
    interface IEntityMappingConfiguration
    {
        void Map(ModelBuilder b);
    }
}
