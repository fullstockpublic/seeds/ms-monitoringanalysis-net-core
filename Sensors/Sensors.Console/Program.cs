﻿using Bogus;
using Sensors.Business;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Sensors.Cmd
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Started");
            DotNetEnv.Env.Load();
            Console.WriteLine();

            List<Guid> sensor = new List<Guid>();
            sensor.Add(new Guid("c49a6dfd-67a9-4cd5-8d45-4342b596854c"));
            sensor.Add(new Guid("8ff12273-5c76-49b0-990a-cb79bc36b80c"));
            sensor.Add(new Guid("f681bb4b-be6e-41c7-b770-770dc181fd77"));
            sensor.Add(new Guid("c1b3d1c5-06a4-4a02-ad4c-455e1b700dae"));

            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = cancellationTokenSource.Token;

            Console.WriteLine($"Sensor: {sensor.Count}");

            while (!cancellationToken.IsCancellationRequested)
            {
                Parallel.ForEach(sensor, new ParallelOptions { MaxDegreeOfParallelism = 4 }, id =>
                {
                    Faker faker = new Faker();

                    MonitoringAnalysis monitoringAnalysis = new MonitoringAnalysis(id);
                    string result = monitoringAnalysis.PostSensorMessage(faker.Random.Number(1, 55), cancellationToken).GetAwaiter().GetResult();

                    Console.WriteLine($"Sensor Name: {id}, Thread Id= {Thread.CurrentThread.ManagedThreadId}, Result Http: {result}");

                });
            }           

            Console.WriteLine("Press enter to stop the task");
            Console.ReadLine();
            cancellationTokenSource.Cancel();

            Console.WriteLine();
            Console.WriteLine("Press any key to close");
            Console.ReadKey();
        }
    }
}
