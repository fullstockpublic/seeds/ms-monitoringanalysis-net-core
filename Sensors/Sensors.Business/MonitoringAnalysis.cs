﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sensors.Business
{
    public class MonitoringAnalysis
    {
        public string url { get; set; }
        public Guid idSensor { get; set; }

        public MonitoringAnalysis(Guid idSensor)
        {
            this.url = Environment.GetEnvironmentVariable("URL-API-MONITORINGANALYSIS");

            if (string.IsNullOrWhiteSpace(this.url))
                throw new Exception("The 'URL-API-MONITORINGANALYSIS' parameter is not found in the environment variables");

            this.idSensor = idSensor;
        }

        public async Task<string> PostSensorMessage(int value, CancellationToken cancellationToken)
        {
            try
            {
                //Create a new instance of HttpClient
                using (var client = new HttpClient())
                using (var request = new HttpRequestMessage(HttpMethod.Post, $"{this.url}/SensorMessage"))
                {
                    var json = JsonConvert.SerializeObject(new { sensor = this.idSensor, value = value });
                    using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
                    {
                        request.Content = stringContent;

                        using (var response = await client
                            .SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return await response.Content.ReadAsStringAsync();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }
    }
}
