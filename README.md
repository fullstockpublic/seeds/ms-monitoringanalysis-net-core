# 

# Micro Service Monitoring Analysis .NET CORE & EF CORE

**Arquitectura**

![Arquitecture](Doc/img/Arquitecture.png)

**Estructura**

* **{ Proyect }.Api**: Api Rest siendo la aplicación encargada de englobar toda la estructura.
* **{ Proyect }.Bussines**: Reglas de negocio y funcionalidades que sean del mismo.
* **{ Proyect }.Dal**: Estructura para la implementación Genérica del ORM.
* **{ Proyect }.Dao**: Implementacion del ORM con el Negocio.
* **{ Proyect }.Domain**: Entidades del Negocio.
* **{ Proyect }.UnitTest**: Implementación de *TDD* y la validación del Código.
* **{ Proyect }.Migration**: Semilla y Herramienta de Versionado en la DB.

La Arquitectura contiene un enfoque en el patrón de diseño por parte de *[Martin Fowler](https://martinfowler.com/)*

**En cual se Remarca:**

* **[Unit of Work](https://martinfowler.com/eaaCatalog/unitOfWork.html)**: Mantiene una lista de objetos afectados por una operación empresarial, coordina la redacción de modificaciones y la resolución de problemas de concurrencia.
* **[Repository](https://martinfowler.com/eaaCatalog/repository.html)**: Media entre el dominio y las capas de los datos utilizando una interfaz similar a una colección para acceder a los objetos del dominio.

* **InvoiceUoW**: Permite que se pueda mantener la separación Repository y UoW a la vez que se pueda agrupar las Entidades involucradas en caso de ser necesario.
* **ManagerContext**: Quien se encarga de contener el contexto de la Db, del *Fluent y la carga dinámica IEntityTypeConfiguration*.
* [Implementing background tasks in .NET Core 2.x ](https://blogs.msdn.microsoft.com/cesardelatorre/2017/11/18/implementing-background-tasks-in-microservices-with-ihostedservice-and-the-backgroundservice-class-net-core-2-x/) permitiendo de forma transparente el **Queue**.

**Esquema Dal-Dao**

![Dao-Dal](Doc/img/Dao-Dal.png)

**El proyecto contiene los siguientes componentes principales:**

* **[Entity Framework Core](https://www.nuget.org/packages/Microsoft.EntityFrameworkCore/2.2.1)**: ORM Utilizado.
* **[EF Core - InMemory](https://www.nuget.org/packages/Microsoft.EntityFrameworkCore.InMemory/2.2.1)**: Utilizado para generar un Contexto sin Base de Datos y utilizando tanto la estructura de Fluent como UoW.
* **[Bogus - (Fake Data)](https://github.com/bchavez/Bogus)**: Información Random Fake, permitiendo mejorar los Unitest o Funcionalidades que requieran dicha información.
* **[Dotnet-env](https://github.com/tonerdo/dotnet-env)**: Variables de entorno mediante archivo '.env', pensado para testeo y soporte de *Dockerizacion*
* **[Swashbuckle - AspNetCore](https://github.com/domaindrivendev/Swashbuckle.AspNetCore)**: Implementacion de Swagger en el proyecto Asp.Net Core.

**Environment Variables**

Si está trabajando en un entorno local, utilice el archivo `.env`.
En caso de publicar la aplicación en un entorno productivo, configúrela a través de las "variables de entorno".

**File** 
            
    .env

* TIMEAVERAGE= Tiempo limitado (defecto 30 segundos)
* S-CONSTANTE= Constancia s
* M-CONSTANTE= Constancia m

**Swagger - Testing & documentacion**

URL: http://localhost:5000/swagger/index.html

![swagger](Doc/img/swagger.png)

-------------------
Know Your Foe: Would You Like to Know **More**?

* *[Using IoC Container with Entity Framework Repository Pattern](https://codereview.stackexchange.com/questions/159074/using-ioc-container-with-entity-framework-repository-pattern)*. Aunque no se fuera a utilizar el *Container*, pero la base de **InvoiceUoW y IHasPKey(Nombrada 'BaseEntity')** fue implementada.
* También en el Libro [Patterns of Enterprise Application Architecture](http://training.hasintech.com/download/attachments/1802696/Patterns%20of%20Enterprise%20Application%20Architecture%20-%20Martin%20Fowler.pdf?version=1&modificationDate=1464702352696&api=v2) en la que se basó la Arquitectura.

